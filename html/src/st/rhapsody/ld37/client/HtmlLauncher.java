package st.rhapsody.ld37.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import st.rhapsody.ld37.LD37;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(960, 720);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                return new LD37();
        }
}