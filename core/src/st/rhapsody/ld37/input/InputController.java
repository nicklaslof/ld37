package st.rhapsody.ld37.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

/**
 * Created by nicklas on 2016-12-10.
 */
public class InputController implements InputProcessor {
    private int mouseLocationX;
    private int mouseLocationY;
    private boolean leftPressed;
    private boolean rightPressed;
    private boolean upPressed;
    private boolean downPressed;
    private boolean leftButtonPressed;
    private boolean leftButtonContiousPressed;
    private boolean paused;
    private boolean noMusic;

    public void tick(){
    }

    @Override
    public boolean keyDown(int keycode) {

        boolean processed = false;


        if (keycode == Input.Keys.A){
            processed = leftPressed = true;
        }
        if (keycode == Input.Keys.D){
            processed = rightPressed = true;
        }
        if (keycode == Input.Keys.W){
            processed = upPressed = true;
        }
        if (keycode == Input.Keys.S){
            processed = downPressed = true;
        }

        if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.P){
            Gdx.input.setCursorCatched(false);
            processed = true;
            paused = true;
        }

        return processed;
    }

    @Override
    public boolean keyUp(int keycode) {

        boolean processed = false;


        if (keycode == Input.Keys.A){
            leftPressed = false;
            processed = true;
        }
        if (keycode == Input.Keys.D){
            rightPressed = false;
            processed = true;
        }
        if (keycode == Input.Keys.W){
            upPressed = false;
            processed = true;
        }
        if (keycode == Input.Keys.S){
            downPressed = false;
            processed = true;
        }

        if (keycode == Input.Keys.M){
            noMusic = !noMusic;
            processed = true;
        }

        return processed;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        boolean processed = false;
        if (button == Input.Buttons.LEFT){
            if (paused){
                Gdx.input.setCursorCatched(true);
                paused = false;
            }else {
                leftButtonPressed = true;
            }
            processed = true;
        }

        return processed;

    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        boolean processed = false;
        if (button == Input.Buttons.LEFT){
            leftButtonPressed = false;
            processed = false;
        }

        return processed;

    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        mouseLocationX = screenX;
        mouseLocationY = screenY;
        return true;

    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        mouseLocationX = screenX;
        mouseLocationY = screenY;
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public int getMouseLocationX() {
        return mouseLocationX;
    }

    public int getMouseLocationY() {
        return mouseLocationY;
    }

    public boolean isLeftPressed() {
        return leftPressed;
    }

    public boolean isRightPressed() {
        return rightPressed;
    }

    public boolean isUpPressed() {
        return upPressed;
    }

    public boolean isDownPressed() {
        return downPressed;
    }

    public boolean isLeftButtonPressed() {
        return leftButtonPressed;
    }

    public boolean isLeftButtonContiousPressed() {
        return leftButtonContiousPressed;
    }

    public boolean isPaused() {
        return paused;
    }

    public boolean mute() {
        return noMusic;
    }
}
