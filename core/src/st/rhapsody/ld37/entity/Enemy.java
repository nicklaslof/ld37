package st.rhapsody.ld37.entity;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld37.entity.powerup.*;
import st.rhapsody.ld37.level.Level;

/**
 * Created by nicklas on 2016-12-10.
 */
public class Enemy extends Entity{
    private Vector2 heroPositionTmp = new Vector2();
    private Vector2 myPositionTmp = new Vector2();
    private Powerup powerupToDrop;
    protected int score;

    public Enemy(Sprite sprite, int width, int height) {
        super(sprite, width, height);
        mob = true;
    }

    public Enemy(Sprite sprite, int width, int height, TextureAtlas textureAtlas, String[] idleFrames, String[] walkingFrames, float idleSpeed, float walkingSpeed) {
        super(sprite, width, height, textureAtlas, idleFrames, walkingFrames, idleSpeed, walkingSpeed);
        mob = true;
    }

    @Override
    public void tick(float delta) {
        super.tick(delta);
        Hero hero = Level.hero;
        hero.getPosition(heroPositionTmp);

        getPosition(myPositionTmp);

        if (heroPositionTmp.x < myPositionTmp.x){
            lookLeft();
        }else{
            lookRight();
        }

        heroPositionTmp.sub(myPositionTmp).nor();
        transpose(heroPositionTmp.x*20, heroPositionTmp.y*20);
    }

    @Override
    protected void dropPickup() {

        int random = MathUtils.random(1, 5);
        if (random == 1){
            int random1 = MathUtils.random(1, 5);
            if (random1 == 1){
                powerupToDrop = new HealthUp();
            }else if (random1 == 2){
                powerupToDrop = new FlameThrower();
            }else if (random1 == 3){
                powerupToDrop = new DoubleSpeed();
            }else if (random1 == 4){
                powerupToDrop = new WalkSpeed();
            }else if (random1 == 5){
                powerupToDrop = new Laser();
            }
        }

       // powerupToDrop = new FlameThrower();

    }

    public Powerup getPowerupToDrop() {
        return powerupToDrop;
    }

    public int getScore() {
        return score;
    }
}
