package st.rhapsody.ld37.entity.particle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld37.entity.Hero;
import st.rhapsody.ld37.level.Level;

/**
 * Created by nicklas on 2016-12-11.
 */
public class HeartParticle extends Particle{

    private float timeAlive;
    private float timeToLive;

    private final Vector2 tmpVector = new Vector2();
    public HeartParticle(float timeToLive, int offset) {
        super(new Sprite(new Texture(Gdx.files.internal("heart.png"))), timeToLive, offset, 10.5f);
        this.timeToLive = timeToLive;
    }

    @Override
    public void doTick(float delta) {
        Hero hero = Level.hero;
        hero.getPosition(tmpVector);
        scale(4f + (timeAlive*30));
        setPosition(tmpVector.x+8, tmpVector.y+24);

        timeAlive += delta;

        if (timeAlive >= timeToLive) {
            setDispose(true);
        }

    }
}
