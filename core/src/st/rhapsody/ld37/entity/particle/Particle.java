package st.rhapsody.ld37.entity.particle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import st.rhapsody.ld37.entity.Entity;

/**
 * Created by nicklas on 2016-12-10.
 */
public class Particle extends Entity {
    private float timeAlive;
    private float timeToLive;
    private final float offset;
    private float initialScale = 0;

    public Particle(Color color, float timeToLive, int offset) {
        super(new Sprite(new Texture(Gdx.files.internal("particle.png"))),1,1);
        this.timeToLive = timeToLive;
        this.offset = offset/10f;
        this.setColor(color);
        initialScale = 0;
    }

    public Particle(Sprite sprite, float timeToLive, int offset, float initialScale) {
        super(sprite,1,1);
        this.timeToLive = timeToLive;
        this.offset = offset/10f;
        this.initialScale = initialScale;
    }

    public void doTick(float delta){
        timeAlive += delta;

        if (timeAlive >= timeToLive) {
            setDispose(true);
        }

        float scale = initialScale + (timeToLive - timeAlive);


        if (scale > initialScale + 1.4) {
            transpose(2.5f * (offset + MathUtils.random(-2f, 2f) / (timeAlive)),
                    2.5f * (offset + MathUtils.random(-2f, 2f) / (timeAlive)));
            scale(scale);
        }
    }


    @Override
    public void tick(float delta) {

        doTick(delta);
        super.tick(delta);
    }

}
