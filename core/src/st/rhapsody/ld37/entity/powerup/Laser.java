package st.rhapsody.ld37.entity.powerup;

import com.badlogic.gdx.graphics.g2d.Sprite;
import st.rhapsody.ld37.entity.Powerup;
import st.rhapsody.ld37.resource.GameTextures;

/**
 * Created by nicklas on 2016-12-12.
 */
public class Laser  extends Powerup {
    public Laser() {
        super(new Sprite(), 16, 16, GameTextures.powerups, new String[]{"l_1", "l_2"},
                new String[]{"l_1", "l_2"},
                0.1f, 0.1f, 10, 5);
        scale(1.6f);


    }
}
