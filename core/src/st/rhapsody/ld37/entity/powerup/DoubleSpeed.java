package st.rhapsody.ld37.entity.powerup;

import com.badlogic.gdx.graphics.g2d.Sprite;
import st.rhapsody.ld37.entity.Powerup;
import st.rhapsody.ld37.resource.GameTextures;

/**
 * Created by nicklas on 2016-12-11.
 */
public class DoubleSpeed extends Powerup{
    public DoubleSpeed() {
        super(new Sprite(), 16, 16, GameTextures.powerups, new String[]{"d_1", "d_2"},
                new String[]{"d_1", "d_2"},
                0.1f, 0.1f, 10, 7);
        scale(1.6f);


    }
}
