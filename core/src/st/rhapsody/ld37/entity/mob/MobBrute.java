package st.rhapsody.ld37.entity.mob;

import com.badlogic.gdx.graphics.g2d.Sprite;
import st.rhapsody.ld37.entity.Enemy;
import st.rhapsody.ld37.resource.GameAudio;
import st.rhapsody.ld37.resource.GameTextures;

/**
 * Created by nicklas on 2016-12-10.
 */
public class MobBrute extends Enemy {

    public MobBrute() {
        super(new Sprite(), 16, 16, GameTextures.mobBrute, new String[]{"brute_1", "brute_2", "brute_3", "brute_4"},
                new String[]{"brute_1", "brute_2", "brute_3", "brute_4"},
                0.1f, 0.1f);
        scale(1.6f);

        health = 3;
        score = 10;
    }

    @Override
    public void onHurt() {
        GameAudio.brute_hit.play(0.3f);
    }
}
