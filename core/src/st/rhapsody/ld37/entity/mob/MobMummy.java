package st.rhapsody.ld37.entity.mob;

import com.badlogic.gdx.graphics.g2d.Sprite;
import st.rhapsody.ld37.entity.Enemy;
import st.rhapsody.ld37.resource.GameAudio;
import st.rhapsody.ld37.resource.GameTextures;

/**
 * Created by nicklas on 2016-12-11.
 */
public class MobMummy extends Enemy{
    public MobMummy() {
        super(new Sprite(), 16, 16, GameTextures.mobMummy, new String[]{"mummy_1", "mummy_2"},
                new String[]{"mummy_1", "mummy_2"},
                0.1f, 0.1f);
        scale(1.6f);
        health = 1;
        score = 5;
    }

    @Override
    public void onHurt() {
        GameAudio.mummy_hit.play(0.3f);
    }
}
