package st.rhapsody.ld37.entity.mob;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import st.rhapsody.ld37.entity.Enemy;
import st.rhapsody.ld37.resource.GameAudio;
import st.rhapsody.ld37.resource.GameTextures;

/**
 * Created by nicklas on 2016-12-11.
 */
public class MobTheMan extends Enemy{
    public MobTheMan() {
        super(new Sprite(), 16, 16, GameTextures.mobTheMan, new String[]{"theman_1", "theman_2","theman_3", "theman_4"},
                new String[]{"theman_1", "theman_2","theman_3", "theman_4"},
                0.2f, 0.2f);
        scale(1.6f);
        health = 4;
        score = 15;
    }

    @Override
    public void onHurt() {
        GameAudio.man_hit.play(0.3f);

    }
}
