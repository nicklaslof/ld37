package st.rhapsody.ld37.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld37.level.Level;

/**
 * Created by nicklas on 2016-12-10.
 */
public class Entity {
    private final boolean animated;
    private Animation idleAnimation;
    private Array<TextureRegion> idleFrames;
    private Animation walkingAnimation;
    private Array<TextureRegion> walkingFrames;
    private TextureAtlas textureAtlas;
    private Sprite sprite;
    protected Vector2 pos = new Vector2();
    private Vector2 tmpVector = new Vector2();
    private float transposeX;
    private float transposeY;
    private float scale = 1;
    private float rotation = 0;
    private boolean dispose;
    private float stateTime;
    private TextureRegion currentFrame;
    private boolean moving;

    private Rectangle rectangle = new Rectangle();
    private Rectangle tmpRectangle = new Rectangle();
    protected boolean isBullet = false;
    protected boolean mob = false;
    private Color color;
    private boolean died;


    protected int health = 1;
    private float hurtCountdown;
    private boolean hurt;
    private boolean lookLeft;
    private boolean lookRight;
    protected float entityHurtCountdown = 0.2f;

    public Entity(Sprite sprite, int width, int height) {
        this.sprite = sprite;
        this.animated = false;
        sprite.setSize(width, height);
        sprite.setOriginCenter();
        rectangle.setSize(width, height);
    }
    public Entity(Sprite sprite, int width, int height, TextureAtlas textureAtlas, String[] idleFrames, String[] walkingFrames, float idleSpeed, float walkingSpeed) {
        this.sprite = sprite;
        this.textureAtlas = textureAtlas;


        this.walkingFrames = new Array<TextureRegion>();

        for (String walkingFrame : walkingFrames) {
            TextureAtlas.AtlasRegion region = textureAtlas.findRegion(walkingFrame);
            this.walkingFrames.add(region);
        }

        walkingAnimation = new Animation(walkingSpeed, this.walkingFrames, Animation.PlayMode.LOOP);

        this.idleFrames = new Array<TextureRegion>();

        for (String idleFrame : idleFrames) {
            TextureAtlas.AtlasRegion region = textureAtlas.findRegion(idleFrame);
            this.idleFrames.add(region);
        }

        idleAnimation = new Animation(idleSpeed, this.idleFrames, Animation.PlayMode.LOOP);


        this.animated = true;
        sprite.setSize(width, height);
        rectangle.setSize(width, height);
        sprite.setOriginCenter();
    }

    public void tick(float delta){
        sprite.setRotation(rotation);
        pos.add(transposeX*delta, transposeY*delta);
        rectangle.setPosition(pos);
        sprite.setScale(scale);

        transposeX = 0;
        transposeY = 0;


        if (pos.x < 0 || pos.y > Gdx.graphics.getWidth() || pos.y < 0 || pos.y > Gdx.graphics.getHeight()){
            dispose = true;
        }

        if (animated){
            stateTime += delta;
            if (moving) {
                currentFrame = walkingAnimation.getKeyFrame(stateTime, true);
            }else{
                currentFrame = idleAnimation.getKeyFrame(stateTime, true);
            }
        }

        hurtCountdown -= delta;

    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void transpose(float x, float y){
        transposeX = x;
        transposeY = y;
    }

    public void scale (float y){
        scale = y;
    }

    public void rotate(float rot){
        rotation = rot;
    }

    public void render(SpriteBatch spriteBatch){

        sprite.setPosition(pos.x, pos.y);

        if (animated){
            if (currentFrame != null) {
                sprite.setRegion(currentFrame);
            }
        }

        if (lookLeft){
            sprite.setFlip(true, false);
        }

        if (lookRight){
            sprite.setFlip(false, false);
        }
        try {
            sprite.draw(spriteBatch);
        }catch (Exception ex){

        }
    }


    public void setPosition(float x, float y){
        pos.set(x, y);
    }

    public void getPosition(Vector2 position) {
        position.set(pos);
    }

    public float getRotation() {
        return sprite.getRotation();
    }

    public boolean isDispose() {
        return dispose;
    }

    public void setDispose(boolean dispose) {
        this.dispose = dispose;
        //System.out.println("dispose:" +this);
    }

    public void collidedWith(Entity e) {
        //System.out.println(e);
        if (e.isBullet){
            hurt(1);
            e.setDispose(true);
        }

        if (e instanceof Hero){
            e.hurt(1);
        }
    }

    public void hurt(int ammount) {
        if (hurtCountdown <= 0.0f) {
            hurt = true;
            onHurt();
            health -= ammount;
            if (health < 1) {
                died = true;
                setDispose(true);

                dropPickup();
            }

            hurtCountdown = entityHurtCountdown;
        }
    }

    protected void onHurt() {

    }

    protected void dropPickup() {

    }

    public boolean isMob() {
        return mob;
    }

    public boolean isHurt() {
        return hurt;
    }

    public boolean hasDied() {
        return died;
    }

    public void resetHurt() {
        this.hurt = false;
    }

    public void setColor(Color color) {
        this.color = color;
        sprite.setColor(color);
    }

    public int getHealth() {
        return health;
    }


    public void lookLeft() {
        lookLeft = true;
        lookRight = false;
    }

    public void lookRight() {
        lookLeft = false;
        lookRight = true;
    }

    public boolean canMove(float x, float y) {
        getPosition(tmpVector);
        tmpVector.add(x,y);
        tmpRectangle.setPosition(tmpVector);
        tmpRectangle.setSize(16,16);
        return !Level.checkTileCollision(tmpRectangle);
    }

    public void setDied(boolean died) {
        this.died = died;
    }

    public float getScale() {
        return scale;
    }

    public void setPosition(Vector2 spawnLocation) {
        pos.set(spawnLocation);
    }
}
