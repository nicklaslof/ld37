package st.rhapsody.ld37.entity;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import st.rhapsody.ld37.resource.GameAudio;

/**
 * Created by nicklas on 2016-12-11.
 */
public class Powerup extends Entity{

    private float timeToLiveCounter;
    private final float powerTimeToLive;
    private float powerTimeToLiveCounter;
    private boolean pickedUp = false;
    private boolean expired;


    public Powerup(Sprite sprite, int width, int height, TextureAtlas textureAtlas, String[] idleFrames,
                   String[] walkingFrames, float idleSpeed, float walkingSpeed, float timeToLive, float powerTimeToLive) {
        super(sprite, width, height, textureAtlas, idleFrames, walkingFrames, idleSpeed, walkingSpeed);

        this.timeToLiveCounter = timeToLive;
        this.powerTimeToLive = powerTimeToLive;
    }

    @Override
    public void collidedWith(Entity e) {
        if (e instanceof Hero) {
            if (!isDispose()) {
                ((Hero) e).setPowerup(this);
                //if (!inputController.mute()) {
                GameAudio.pickup.play(0.3f);
                // }
            }
            setDied(true);
            setDispose(true);
        }
    }

    @Override
    public void tick(float delta) {
        super.tick(delta);
        timeToLiveCounter -= delta;

        if (timeToLiveCounter <= 0f){
            setDispose(true);
        }

        if (pickedUp){
            powerTimeToLiveCounter -= delta;
            if (powerTimeToLiveCounter <= 0f){
                expired = true;
            }
        }
    }

    protected void onPickup(){
        powerTimeToLiveCounter = powerTimeToLive;
        pickedUp = true;
    }

    public boolean isExpired() {
        return expired;
    }

    public void resetTimeToLive() {
        this.timeToLiveCounter = 5f;
    }
}
