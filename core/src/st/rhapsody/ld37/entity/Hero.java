package st.rhapsody.ld37.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld37.entity.powerup.HealthUp;
import st.rhapsody.ld37.resource.GameAudio;
import st.rhapsody.ld37.resource.GameTextures;

/**
 * Created by nicklas on 2016-12-10.
 */

public class Hero extends Entity{
    private Powerup powerup;
    private int score = 0;

    public Hero() {
        super(new Sprite(), 16, 16, GameTextures.hero, new String[]{"hero_3", "hero_5", "hero_6", "hero_5"},
                new String[]{"hero_1", "hero_2", "hero_3", "hero_4"}, 0.2f, 0.1f);
        this.health = 5;
        this.entityHurtCountdown = 1.0f;
    }

    @Override
    public void tick(float delta) {
        super.tick(delta);

        if (powerup != null){
            powerup.tick(delta);
            if (powerup.isExpired()){
                powerup = null;
            }
        }
    }

    public void setPowerup(Powerup powerup) {
        //Check if powerup is health.. then don't set it
        if (powerup instanceof HealthUp){
            heal(1);
        }else {
            this.powerup = powerup;
            powerup.onPickup();
        }
    }

    private void heal(int ammount) {
        if (health < 5){
            health++;
        }
    }

    @Override
    public void onHurt() {
        GameAudio.hero_hit.play(0.3f);
    }

    public Powerup getPowerup() {
        return powerup;
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        super.render(spriteBatch);
    }

    public void addScore(int score) {
        this.score += score;
    }

    public int getScore() {
        return score;
    }
}
