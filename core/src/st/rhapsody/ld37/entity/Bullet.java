package st.rhapsody.ld37.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by nicklas on 2016-12-10.
 */
public class Bullet extends Entity{
    private final Vector2 direction = new Vector2();
    private float timeToLiveCounter = 0.2f;

    public Bullet(Vector2 position, Vector2 direction) {
        super(new Sprite(new Texture(Gdx.files.internal("bullet.png"))),4,4);
        this.pos.set(position);
        this.direction.set(direction);
        this.isBullet = true;

    }

    public void setTimeToLive(float timeToLive) {
        this.timeToLiveCounter = timeToLive;
    }

    @Override
    public void tick(float delta) {

        timeToLiveCounter -= delta;
        if (timeToLiveCounter <= 0.0f){
            setDispose(true);
        }else {
            boolean canMove = canMove((direction.x * 250)*delta, (direction.y * 250)*delta);
            if (canMove) {
                transpose(direction.x * 500, direction.y * 500);
            }else{
                setDispose(true);
            }

        }
        super.tick(delta);
    }
}
