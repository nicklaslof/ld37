package st.rhapsody.ld37;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import st.rhapsody.ld37.resource.GameAudio;
import st.rhapsody.ld37.resource.GameTextures;
import st.rhapsody.ld37.screen.FontDrawer;
import st.rhapsody.ld37.screen.GameScreen;
import st.rhapsody.ld37.screen.InstructionsScreen;
import st.rhapsody.ld37.screen.IntroScreen;

public class LD37 extends Game {

	private static boolean startGame;
	private static boolean showIntro;
	private static boolean showInstructions;
	private Screen currentScreen;
	private GameTextures gameTextures;
	private GlyphLayout layout;
	private BitmapFont font;
	private FontDrawer fontDrawer;
	private GameAudio gameAudio;

	@Override
	public void create() {
		fontDrawer = new FontDrawer();
		gameTextures = new GameTextures();
		gameAudio = new GameAudio();
		currentScreen = new IntroScreen();
		setScreen(currentScreen);
	}

	@Override
	public void dispose() {
		gameAudio.dispose();
		currentScreen.dispose();
		fontDrawer.dispose();
		gameTextures.dispose();
	}

	public static void startGame() {
		startGame = true;
	}


	public static void showIntro() {
		showIntro = true;
	}

	@Override
	public void render() {
		super.render();

		if (startGame){
			currentScreen.dispose();
			currentScreen = new GameScreen();
			setScreen(currentScreen);
			startGame = false;
		}

		if (showInstructions){
			currentScreen.dispose();
			currentScreen = new InstructionsScreen();
			setScreen(currentScreen);
			showInstructions = false;
		}

		if (showIntro){
			currentScreen.dispose();
			currentScreen = new IntroScreen();
			setScreen(currentScreen);
			showIntro = false;
		}


	}

	public static void showInstructions() {
		showInstructions = true;

	}
}
