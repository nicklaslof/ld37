package st.rhapsody.ld37.level;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by nicklas on 2016-12-10.
 */
public class Tile {

    private final int x;
    private final int y;
    private final Sprite sprite;
    private final Rectangle rectangle;
    private final Sprite overlaySprite;

    public Tile(int x, int y, Sprite sprite, Sprite overlaySprite) {
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        this.overlaySprite = overlaySprite;
        rectangle = new Rectangle(x*16, y*16, 16,16);
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void render(SpriteBatch spriteBatch) {
        this.sprite.setPosition(x*16, y*16);
        this.sprite.draw(spriteBatch);
        if (overlaySprite != null){
            this.overlaySprite.setPosition(x*16, y*16);
            this.overlaySprite.draw(spriteBatch);
        }
    }
}
