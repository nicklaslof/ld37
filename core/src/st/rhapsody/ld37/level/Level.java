package st.rhapsody.ld37.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld37.entity.*;
import st.rhapsody.ld37.entity.particle.HeartParticle;
import st.rhapsody.ld37.entity.particle.Particle;
import st.rhapsody.ld37.entity.powerup.*;
import st.rhapsody.ld37.input.InputController;
import st.rhapsody.ld37.resource.GameAudio;
import st.rhapsody.ld37.resource.GameTextures;

/**
 * Created by nicklas on 2016-12-10.
 */
public class Level {

    private final MobSpawner mobSpawner;
    public static Hero hero = new Hero();
    private Array<Entity> entities = new Array<Entity>();
    private Array<Entity> bullets = new Array<Entity>();
    private Array<Entity> particles = new Array<Entity>();

    private Array<Entity> disposableEntities = new Array<Entity>();



    private InputController inputController;
    private Vector2 mouseLocation = new Vector2();
    private Vector2 playerPosition = new Vector2();

    private float playerSpeed = 65f;
    private Array<Entity> tempArray = new Array<Entity>();
    private float currentBulletDelay = 0.0f;
    private float bulletDelay = 0.5f;
    private Vector2 tmpVector = new Vector2();

    private Sprite bottomRow = new Sprite(new Texture(Gdx.files.internal("background.png")));
    private Sprite walls = new Sprite(new Texture(Gdx.files.internal("wall.png")));
    private Sprite bed = new Sprite(GameTextures.roomStuff.findRegion("bed"));
    private Sprite bookCase = new Sprite(GameTextures.roomStuff.findRegion("bookcase"));
    private Sprite aim = new Sprite(new Texture(Gdx.files.internal("aim.png")));


    private Sprite help = new Sprite(GameTextures.tags.findRegion("help"));
    private Sprite count = new Sprite(GameTextures.tags.findRegion("count"));
    private Sprite fine = new Sprite(GameTextures.tags.findRegion("fine"));
    private Sprite no_monster = new Sprite(GameTextures.tags.findRegion("no_monster"));
    private Sprite cake = new Sprite(GameTextures.tags.findRegion("cake"));
    private Sprite theyare = new Sprite(GameTextures.tags.findRegion("theyare"));
    private Sprite afterme = new Sprite(GameTextures.tags.findRegion("afterme"));
    private static Tile[] tiles;
    private static Sprite[] floorSprites;

    public Level(InputController inputController) {
        hero = new Hero();
        tiles = new Tile[(320/16) * (240/16)];
        floorSprites = new Sprite[(320/16) * (240/16)];
        this.inputController = inputController;
        mobSpawner = new MobSpawner(this);

        aim.setScale(0.5f);

        Color tagColor = new Color(0.5f,0,0,0.7f);

        help.setSize(16,16);
        help.setColor(tagColor);
        count.setSize(16,16);
        count.setColor(tagColor);
        fine.setSize(16,16);
        fine.setColor(tagColor);
        no_monster.setSize(16,16);
        no_monster.setColor(tagColor);
        cake.setSize(16,16);
        cake.setColor(tagColor);
        theyare.setSize(16,16);
        theyare.setColor(tagColor);
        afterme.setSize(16,16);
        afterme.setColor(tagColor);

        for (int x = 0; x < 320/16; x++) {
            for (int y = 1; y < 240 / 16; y++) {
                boolean wallAdded = false;
                if (y == 1 || y == (240/16)-1){
                    addWall(x, y);
                    wallAdded = true;
                }
                if (x == 0 || x == (320/16)-1){
                    if (y != 0) {
                        addWall(x, y);
                        wallAdded = true;
                    }
                }

                /*if (!wallAdded && MathUtils.random(100) == 1){
                    if (x != (320/16)/2 && y != (240/16)/2) {
                        setTileAt(new Tile(x, y, bed, null), x, y);
                    }
                }*/

                int floorTextureChance = MathUtils.random(1, 8);
                if (floorTextureChance == 1) {
                    Sprite sprite = new Sprite();
                    int random = MathUtils.random(1, 4);
                    TextureAtlas.AtlasRegion texture = GameTextures.floorTiles.findRegion(String.valueOf(random));
                    sprite.setRegion(texture);
                    sprite.setPosition(x * 16, y * 16);
                    sprite.setSize(16, 16);
                    setFloorTileAt(sprite, x, y);
                }
            }
        }

        hero.setPosition(320/2, 240/2);
    }

    private void addWall(int x, int y){
        Sprite overlay = null;
        int random = MathUtils.random(1, 40);
        if (random < 8){
            if (random == 1) overlay = help;
            if (random == 2) overlay = count;
            if (random == 3) overlay = fine;
            if (random == 4) overlay = no_monster;
            if (random == 5) overlay = cake;
            if (random == 6) overlay = theyare;
            if (random == 7) overlay = afterme;
        }
        Tile tile = new Tile(x, y, walls, overlay);
        setTileAt(tile, x, y);
    }

    private void setTileAt(Tile tile, int x, int y){
        tiles[y * (320/16) + x] = tile;
    }
    private void setFloorTileAt(Sprite sprite, int x, int y){
        floorSprites[y * (320/16) + x] = sprite;
    }

    private Tile getTileAt(int x, int y){
        Tile tile = tiles[y * (320/16) + x];
        return tile;
    }


    public void tick(float delta){

        if (inputController.isPaused() || hero.hasDied()){
            inputController.tick();
            return;
        }

        //playerSpeed = 90f;

        //if (hero.hasDied()){
       //     System.out.println("GAME OVER");
      //      return;
     //   }


        mobSpawner.tick(delta);
        tickPlayer(delta);

        currentBulletDelay += delta;

        if (inputController.isLeftButtonPressed() && currentBulletDelay >= bulletDelay){
            Powerup powerup = hero.getPowerup();
            currentBulletDelay = 0.0f;
            float v = MathUtils.atan2(mouseLocation.y - playerPosition.y, mouseLocation.x - playerPosition.x);

            //hero.rotate(MathUtils.radiansToDegrees * v);
            Vector2 direction = new Vector2(MathUtils.cosDeg(MathUtils.radiansToDegrees * v), MathUtils.sinDeg(MathUtils.radiansToDegrees * v)).nor();
            hero.getPosition(playerPosition);
            Bullet bullet = new Bullet(playerPosition, direction);
            bullet.setTimeToLive(0.4f);

            if (powerup instanceof FlameThrower){

                int random = MathUtils.random(1, 3);
                if (random == 1){
                    bullet.setColor(Color.RED);
                }else if (random == 2){
                    bullet.setColor(Color.ORANGE);
                }else if (random == 3){
                    bullet.setColor(Color.YELLOW);
                }

                random = MathUtils.random(2, 5);

                bullet.scale(random);

                bullet.setTimeToLive(0.20f);

                bulletDelay = 0.03f;
                    GameAudio.flame_shoot.play(0.2f);
            }else{
                bullet.scale(1f);
                bullet.setColor(Color.WHITE);

                if (powerup instanceof DoubleSpeed) {
                    bulletDelay = 0.35f;
                    GameAudio.pistol_shoot.play(0.3f);
                }else if (powerup instanceof Laser){
                    bulletDelay = 0.05f;
                    bullet.setColor(Color.GREEN);
                    //if (!inputController.mute()) {
                        GameAudio.laser_shoot.play(0.03f);
                   // }
                }else {
                    bulletDelay = 0.5f;
                    //if (!inputController.mute()) {
                        GameAudio.pistol_shoot.play(0.3f);
                    // }
                }
            }


            addBullet(bullet);
        }

        if (tempArray.size > 0){
            tempArray.clear();
        }
        tempArray.addAll(entities);
        tempArray.add(hero);
        tempArray.addAll(bullets);


        for (Entity entity : entities) {
            entity.tick(delta);

            if (entity.hasDied()){
                if (entity instanceof HealthUp){
                    Particle particle = new HeartParticle(0.5f, 0);
                    particle.setPosition(tmpVector.x + MathUtils.random(-3, 3), tmpVector.y + MathUtils.random(-3, 3));
                    addParticle(particle);
                }else {
                    entity.getPosition(tmpVector);
                    for (int i = 0; i < 20; i++) {
                        Particle particle = new Particle(Color.RED, MathUtils.random(1, 3), i);
                        particle.setPosition(tmpVector.x + MathUtils.random(-3, 3), tmpVector.y + MathUtils.random(-3, 3));
                        addParticle(particle);
                    }
                    if (entity instanceof Enemy) {
                        int score = ((Enemy)entity).getScore();
                        hero.addScore(score);
                        Powerup pickupToDrop = ((Enemy) entity).getPowerupToDrop();
                        if (pickupToDrop != null) {
                            pickupToDrop.setPosition(tmpVector.x, tmpVector.y);
                            if (pickupToDrop != null) {
                                addPowerup(pickupToDrop);
                            }
                        }
                        //if (!inputController.mute()) {
                        GameAudio.mob_dead.play(0.3f);
                        // }
                    }
                }

            }

            if (entity.isHurt()){
                entity.resetHurt();
                entity.getPosition(tmpVector);
                for (int i = 0; i < 4; i++) {
                    Particle particle = new Particle(Color.RED, MathUtils.random(1,3), i);
                    particle.setPosition(tmpVector.x+MathUtils.random(-2,2), tmpVector.y+MathUtils.random(-2,2));
                    addParticle(particle);
                }

            }

            if (entity.isDispose()){
                disposableEntities.add(entity);
            }
        }

        for (Entity bullet : bullets) {
            bullet.tick(delta);
            if (bullet.isDispose()){
                disposableEntities.add(bullet);
            }
        }

        for (Entity particle : particles) {
            particle.tick(delta);
            if (particle.isDispose()){
                disposableEntities.add(particle);
            }
        }

        for (Entity entity : entities) {
            for (Entity e : tempArray) {
                if (e != entity && !e.isDispose()){
                    Rectangle r1 = e.getRectangle();
                    Rectangle r2 = entity.getRectangle();
                    if (r1.overlaps(r2)){
                        entity.collidedWith(e);
                    }
                }
            }
        }

        entities.removeAll(disposableEntities,false);
        bullets.removeAll(disposableEntities, false);
        particles.removeAll(disposableEntities, false);

        inputController.tick();
    }

    private void addPowerup(Powerup pickupToDrop) {
        entities.add(pickupToDrop);
    }

    private void addParticle(Particle particle) {
        particles.add(particle);
    }


    private void tickPlayer(float delta) {


        float x = 0;
        float y = 0;

        float speedMultiplier = 1.0f;

        if (hero.getPowerup() != null && hero.getPowerup() instanceof WalkSpeed){
            speedMultiplier = 1.5f;
        }

        if (inputController.isLeftPressed()){
            x -= playerSpeed*speedMultiplier;
        }

        if (inputController.isRightPressed()){
            x += playerSpeed*speedMultiplier;
        }

        if (inputController.isUpPressed()){
            y += playerSpeed*speedMultiplier;
        }

        if (inputController.isDownPressed()){
            y -= playerSpeed*speedMultiplier;
        }

        if (x != 0.0f || y != 0.0f){
            if (hero.canMove(x*delta,y*delta)) {
                hero.transpose(x, y);
                hero.setMoving(true);
            }else{
                hero.setMoving(false);
            }
        }else{
            hero.setMoving(false);
        }


        int screenHeight = Gdx.graphics.getHeight();
        int screenWidth = Gdx.graphics.getWidth();

        float heightRatio = screenHeight/240f;
        float widthRatio = screenWidth/320f;

        mouseLocation.set(inputController.getMouseLocationX()/widthRatio, (Gdx.graphics.getHeight() - inputController.getMouseLocationY())/heightRatio);

        hero.getPosition(playerPosition);


        float v = mouseLocation.x - playerPosition.x;

        if (v < 0.0f){
            hero.lookLeft();
        }else{
            hero.lookRight();
        }

        hero.tick(delta);

        if (hero.isHurt()){
            hero.resetHurt();
            hero.getPosition(tmpVector);
            for (int i = 0; i < 4; i++) {
                Particle particle = new Particle(Color.RED, MathUtils.random(1,3), i);
                particle.setPosition(tmpVector.x+MathUtils.random(-1,1), tmpVector.y+MathUtils.random(-1,1));
                addParticle(particle);
            }

        }
    }

    public void render(SpriteBatch spriteBatch){

        /*for (int x = 0; x < 320; x +=16) {
            for (int y = 0; y < 240; y +=16) {
                bottomRow.setPosition(x,y);
                bottomRow.draw(spriteBatch);
            }
        }*/


        for (Sprite floorSprite : floorSprites) {
            if (floorSprite != null){
                floorSprite.setColor(0.1f,0.1f,0.1f,0.7f);
                floorSprite.draw(spriteBatch);
            }
        }

        for (int x = 0; x < 320/16; x++) {
            bottomRow.setPosition(x*16, 0);
            bottomRow.draw(spriteBatch);
        }


        for (Tile tile : tiles) {
            if (tile != null) {
                tile.render(spriteBatch);
            }
        }


        hero.render(spriteBatch);


        for (Entity entity : entities) {
            entity.render(spriteBatch);
        }

        for (Entity bullet : bullets) {
            bullet.render(spriteBatch);
        }

        for (Entity particle : particles) {
            particle.render(spriteBatch);
        }

        aim.setPosition(mouseLocation.x-8, mouseLocation.y-8);
        aim.draw(spriteBatch);


    }

    private void addBullet(Bullet bullet) {
        bullets.add(bullet);
    }

    public void addMob(Entity entitiy) {
        entities.add(entitiy);
    }

    public static boolean checkTileCollision(Rectangle rect) {
        for (Tile tile : tiles) {
            if (tile != null) {
                if (tile.getRectangle().overlaps(rect)) {
                    //System.out.println("tile: "+ tile.getRectangle() + "  player: "+rect);
                    return true;
                }
            }
        }
        return false;
    }
}
