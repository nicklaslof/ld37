package st.rhapsody.ld37.level;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld37.entity.mob.MobBrute;
import st.rhapsody.ld37.entity.mob.MobMummy;
import st.rhapsody.ld37.entity.mob.MobTheMan;

/**
 * Created by nicklas on 2016-12-10.
 */
public class MobSpawner {

    public static final int TOP = 0;
    public static final int BOTTOM = 1;
    public static final int LEFT = 2;
    public static final int RIGHT = 3;


    private Level level;
    private float timeSinceBrute;
    private float timeSinceMummy;
    private float timeSinceMan;
    private float totalTime = 250;

    public MobSpawner(Level level) {
        this.level = level;

    }

    public void tick(float delta){

        totalTime += delta;

        float multiplier = totalTime / 250;

        timeSinceBrute +=delta;
        if (timeSinceBrute > 4f/multiplier){
            MobBrute mobBrute = new MobBrute();
            mobBrute.setPosition(getSpawnLocation());
            level.addMob(mobBrute);
            timeSinceBrute = 0;
        }

        timeSinceMummy +=delta;
        if (timeSinceMummy > 2f){
            MobMummy mobMummy = new MobMummy();
            mobMummy.setPosition(getSpawnLocation());
            level.addMob(mobMummy);
            timeSinceMummy = 0;
        }

        timeSinceMan +=delta;
        if (timeSinceMan > 8f/multiplier){
            MobTheMan mobTheMan = new MobTheMan();
            mobTheMan.setPosition(getSpawnLocation());
            level.addMob(mobTheMan);
            timeSinceMan = 0;
        }


        //System.out.println(4f/multiplier + "  "+ 8f/multiplier);
    }

    private Vector2 getSpawnLocation() {
        int spawnX = 0;
        int spawnY = 0;
        int spawnLocation = MathUtils.random(0, 3);
        if (spawnLocation == TOP){
            spawnX = MathUtils.random(0,320);
            spawnY = 240;
        }

        if (spawnLocation == BOTTOM){
            spawnX = MathUtils.random(0,320);
            spawnY = 16;
        }

        if (spawnLocation == LEFT){
            spawnX = 0;
            spawnY = MathUtils.random(0,240);
        }

        if (spawnLocation == RIGHT){
            spawnX = 320;
            spawnY = MathUtils.random(0,240);
        }

        return new Vector2(spawnX, spawnY);
    }
}
