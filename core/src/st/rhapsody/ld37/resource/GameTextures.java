package st.rhapsody.ld37.resource;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Created by nicklas on 2016-12-10.
 */
public class GameTextures {


    public static TextureAtlas hero;
    public static TextureAtlas roomStuff;
    public static TextureAtlas mobBrute;
    public static TextureAtlas mobTheMan;
    public static TextureAtlas mobMummy;
    public static TextureAtlas powerups;
    public static TextureAtlas floorTiles;
    public static TextureAtlas tags;

    public GameTextures() {
        hero = loadTextureAtlas("hero2.pack");
        roomStuff = loadTextureAtlas("roomstuff.pack");
        mobBrute = loadTextureAtlas("mob_brute.pack");
        mobMummy = loadTextureAtlas("mob_mummy.pack");
        mobTheMan = loadTextureAtlas("mob_theman.pack");
        powerups = loadTextureAtlas("powerups.pack");
        floorTiles = loadTextureAtlas("floortiles.pack");
        tags = loadTextureAtlas("tags.pack");
    }

    private TextureAtlas loadTextureAtlas(String textureName){
        FileHandle fileHandle = Gdx.files.internal(textureName);
        TextureAtlas textureAtlas = new TextureAtlas(fileHandle);

        return textureAtlas;
    }

    public void dispose() {
        hero.dispose();
        roomStuff.dispose();
        mobBrute.dispose();
        mobMummy.dispose();
        mobTheMan.dispose();
        powerups.dispose();
        floorTiles.dispose();
        tags.dispose();
    }
}
