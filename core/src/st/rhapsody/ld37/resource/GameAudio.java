package st.rhapsody.ld37.resource;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by nicklas on 2016-12-11.
 */
public class GameAudio {

    private final AssetManager assetManager;
    public static Sound magont;
    public static Sound visa;
    public static Sound saw;

    public static Sound laser_shoot;
    public static Sound flame_shoot;
    public static Sound pistol_shoot;
    public static Sound man_hit;
    public static Sound hero_hit;
    public static Sound mummy_hit;
    public static Sound brute_hit;
    public static Sound pickup;
    public static Sound mob_dead;


    public GameAudio() {
        assetManager = new AssetManager();

        assetManager.load("magont.ogg", Sound.class);
        assetManager.load("visa.ogg", Sound.class);
        assetManager.load("saw.ogg", Sound.class);


        assetManager.load("laser_shoot.ogg", Sound.class);
        assetManager.load("flame_shoot.ogg", Sound.class);
        assetManager.load("man_hit.ogg", Sound.class);
        assetManager.load("hero_hit.ogg", Sound.class);
        assetManager.load("mob_dead.ogg", Sound.class);
        assetManager.load("mummy_hit.ogg", Sound.class);
        assetManager.load("brute_hit.ogg", Sound.class);
        assetManager.load("pickup.ogg", Sound.class);
        assetManager.load("pistol_shoot.ogg", Sound.class);



        assetManager.finishLoading();

        magont = assetManager.get("magont.ogg", Sound.class);
        visa = assetManager.get("visa.ogg", Sound.class);
        saw = assetManager.get("saw.ogg", Sound.class);

        laser_shoot = assetManager.get("laser_shoot.ogg", Sound.class);
        flame_shoot = assetManager.get("flame_shoot.ogg", Sound.class);
        man_hit = assetManager.get("man_hit.ogg", Sound.class);
        hero_hit = assetManager.get("hero_hit.ogg", Sound.class);
        mob_dead = assetManager.get("mob_dead.ogg", Sound.class);
        mummy_hit = assetManager.get("mummy_hit.ogg", Sound.class);
        brute_hit = assetManager.get("brute_hit.ogg", Sound.class);
        pickup = assetManager.get("pickup.ogg", Sound.class);
        pistol_shoot = assetManager.get("pistol_shoot.ogg", Sound.class);
    }

    public void dispose() {
        assetManager.dispose();
        magont.dispose();
        visa.dispose();
        saw.dispose();

        laser_shoot.dispose();
        flame_shoot.dispose();
        man_hit.dispose();
        hero_hit.dispose();
        mob_dead.dispose();
        mummy_hit.dispose();
        brute_hit.dispose();
        pickup.dispose();
        pistol_shoot.dispose();
    }
}
