package st.rhapsody.ld37.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld37.LD37;
import st.rhapsody.ld37.entity.Hero;
import st.rhapsody.ld37.entity.Powerup;
import st.rhapsody.ld37.entity.mob.MobBrute;
import st.rhapsody.ld37.entity.mob.MobMummy;
import st.rhapsody.ld37.entity.mob.MobTheMan;
import st.rhapsody.ld37.entity.powerup.*;

/**
 * Created by nicklas on 2016-12-12.
 */
public class InstructionsScreen implements Screen {
    private OrthographicCamera orthographicCamera;
    private SpriteBatch spriteBatch;

    private MobBrute brute = new MobBrute();
    private Hero hero = new Hero();
    private MobMummy mobMummy = new MobMummy();
    private MobTheMan mobTheMan = new MobTheMan();

    private DoubleSpeed doubleSpeed = new DoubleSpeed();
    private WalkSpeed walkSpeed = new WalkSpeed();
    private Laser laser = new Laser();
    private FlameThrower flameThrower = new FlameThrower();
    private HealthUp healthUp = new HealthUp();
    private float clickTimer;


    @Override
    public void show() {
        orthographicCamera = new OrthographicCamera(320, 240);
        spriteBatch = new SpriteBatch();

        clickTimer = 2.0f;
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.1f, 0.1f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.setProjectionMatrix(orthographicCamera.combined);

        spriteBatch.begin();


        FontDrawer.drawTextAt(spriteBatch,"WASD TO MOVE AROUND", 84,210,0.4f, true);
        FontDrawer.drawTextAt(spriteBatch,"AIM AND SHOOT WITH THE MOUSE", 50,200,0.4f, true);
        FontDrawer.drawTextAt(spriteBatch,"ESC OR P TO PAUSE", 92,190,0.4f, true);
        FontDrawer.drawTextAt(spriteBatch,"M TO DISABLE THE MUSIC!!!", 66,180,0.4f, true);


        FontDrawer.drawTextAt(spriteBatch,"POWER UPS", 124,150,0.4f, true);

        doubleSpeed.setPosition(66,110);
        doubleSpeed.resetTimeToLive();
        doubleSpeed.tick(delta);
        doubleSpeed.render(spriteBatch);

        FontDrawer.drawTextAt(spriteBatch,"DOUBLE GUN SPEED", 96,122,0.4f, true);

        walkSpeed.setPosition(66,90);
        walkSpeed.resetTimeToLive();
        walkSpeed.tick(delta);
        walkSpeed.render(spriteBatch);

        FontDrawer.drawTextAt(spriteBatch,"DOUBLE MOVING SPEED", 96,102,0.4f, true);


        flameThrower.setPosition(66,70);
        flameThrower.resetTimeToLive();
        flameThrower.tick(delta);
        flameThrower.render(spriteBatch);

        FontDrawer.drawTextAt(spriteBatch,"FLAME THROWER", 96,82,0.4f, true);

        laser.setPosition(66,50);
        laser.resetTimeToLive();
        laser.tick(delta);
        laser.render(spriteBatch);

        FontDrawer.drawTextAt(spriteBatch,"LASER", 96,62,0.4f, true);





        healthUp.setPosition(66,30);
        healthUp.resetTimeToLive();
        healthUp.tick(delta);
        healthUp.render(spriteBatch);


        FontDrawer.drawTextAt(spriteBatch,"PLUS ONE HEALTH", 96,42,0.4f, true);



        spriteBatch.end();

        clickTimer -= delta;

        if (clickTimer <= 0.0f && (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.isButtonPressed(Input.Buttons.LEFT))){
            LD37.startGame();
        }

    }

    @Override
    public void resize(int width, int height) {
        orthographicCamera.position.set(320/2,240/2,0);
        orthographicCamera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
