package st.rhapsody.ld37.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld37.LD37;
import st.rhapsody.ld37.entity.Hero;
import st.rhapsody.ld37.entity.mob.MobBrute;

/**
 * Created by nicklas on 2016-12-11.
 */
public class IntroScreen implements Screen{
    private OrthographicCamera orthographicCamera;
    private SpriteBatch spriteBatch;

    private MobBrute brute = new MobBrute();
    private Hero hero = new Hero();


    @Override
    public void show() {
        orthographicCamera = new OrthographicCamera(320, 240);
        spriteBatch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.1f,0.1f,0.2f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        brute.tick(delta);
        hero.tick(delta);

        spriteBatch.setProjectionMatrix(orthographicCamera.combined);

        spriteBatch.begin();

        FontDrawer.drawTextAt(spriteBatch,"PANIC ROOM", 80,230,1.0f, false);

        FontDrawer.drawTextAt(spriteBatch,"HELP THE POOR GUY", 92,190,0.4f, true);
        FontDrawer.drawTextAt(spriteBatch,"FIGHT HIS IMAGINARY MONSTERS", 50,180,0.4f, true);




        FontDrawer.drawTextAt(spriteBatch,"A GAME BY NICKLAS LOF", 76,50,0.4f, true);
        FontDrawer.drawTextAt(spriteBatch,"AND MATHIAS LINDFELDT", 76,40,0.4f, true);

        FontDrawer.drawTextAt(spriteBatch,"FOR LUDUM DARE 37", 92,30,0.4f, true);


        brute.setPosition((320/2)-24,240/2);
        brute.lookRight();
        brute.render(spriteBatch);

        hero.setPosition((320/2)+16 ,240/2);
        hero.scale(1.4f);
        hero.lookLeft();
        hero.render(spriteBatch);


        spriteBatch.end();


        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
            LD37.showInstructions();
        }

    }

    @Override
    public void resize(int width, int height) {
        orthographicCamera.position.set(320/2,240/2,0);
        orthographicCamera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
