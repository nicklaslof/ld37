package st.rhapsody.ld37.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by nicklas on 2016-12-11.
 */
public class FontDrawer {

    private static GlyphLayout layout;
    private static BitmapFont font;

    public FontDrawer() {
        layout = new GlyphLayout();
        font = new BitmapFont(Gdx.files.internal("ld37-font.fnt"), Gdx.files.internal("ld37-font.png"), false);
    }

    public static void drawTextAt(SpriteBatch spriteBatch, String text, int x, int y, float scale, boolean noSpacing){

        String textToDraw;

        if (noSpacing){
            textToDraw = text.toUpperCase();
        }else {
            StringBuffer stringBuffer = new StringBuffer(text.length() * 2);

            for (int i = 0; i < text.length(); i++) {
                stringBuffer.append(text.charAt(i));
                stringBuffer.append(" ");
            }
            textToDraw = stringBuffer.toString().trim();
        }

        layout.setText(font, textToDraw);
        font.getData().setScale(scale);
        //System.out.println("Drawing at "+x+" "+y);
        font.draw(spriteBatch, layout, x, y);

        font.getData().setScale(1);

    }

    public void dispose() {
        font.dispose();
    }
}
