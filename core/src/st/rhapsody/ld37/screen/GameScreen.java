package st.rhapsody.ld37.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import st.rhapsody.ld37.LD37;
import st.rhapsody.ld37.entity.Hero;
import st.rhapsody.ld37.input.InputController;
import st.rhapsody.ld37.level.Level;
import st.rhapsody.ld37.resource.GameAudio;

/**
 * Created by nicklas on 2016-12-10.
 */
public class GameScreen implements Screen{
    private SpriteBatch spriteBatch;
    private Level level;
    private InputController inputController;
    private OrthographicCamera orthographicCamera;
    public static int windowHeight;
    private SpriteBatch uiSpriteBatch;
    private UI ui;

    private Sprite overlay = new Sprite(new Texture(Gdx.files.internal("particle.png")));
    public static int windowWidth;
    private long magontId;
    private long visaId;
    private long sawId;
    private float magontTime;
    private float visaTime;
    private float sawTime;
    private boolean noMusic;
    private float fadeUp;
    private boolean fadingUp;

    @Override
    public void show() {
        orthographicCamera = new OrthographicCamera(320, 240);
        spriteBatch = new SpriteBatch();

        uiSpriteBatch = new SpriteBatch();

        inputController = new InputController();
        Gdx.input.setInputProcessor(inputController);

        level = new Level(inputController);
        ui = new UI(level);

        overlay.setAlpha(0.3f);

        overlay.setScale(330);

        overlay.setPosition(320/2, 240/2);

        Gdx.input.setCursorCatched(true);
        Gdx.input.setCursorPosition(320/2, 240/2);

        magontId = GameAudio.magont.loop(0.0f);
        visaId = GameAudio.visa.loop(0.0f);
        sawId = GameAudio.saw.loop(0.0f);

        magontTime = 0.5f;
        visaTime = 3f;
        sawTime = 0.5f;
        fadingUp = true;


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(64f/255f, 70f/255f, 87f/255f, 1);
        //Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.setProjectionMatrix(orthographicCamera.combined);
        uiSpriteBatch.setProjectionMatrix(orthographicCamera.combined);

        ui.tick(delta);
        level.tick(delta);

        renderLevel();
        renderUI();
        tickAudio(delta);
    }

    private void tickAudio(float delta) {


        if (fadingUp && !inputController.isPaused()){
            fadeUp += delta;
            GameAudio.saw.setVolume(sawId,Math.min(fadeUp/144,0.9f));
            GameAudio.visa.setVolume(visaId,Math.min(fadeUp/144,0.25f));
            GameAudio.magont.setVolume(magontId,Math.min(fadeUp/144,0.6f));
            if (fadeUp >= 96){
                fadingUp = false;
            }
        }



        if (inputController.mute() || inputController.isPaused()){
            GameAudio.saw.pause();
            GameAudio.magont.pause();
            GameAudio.visa.pause();
            return;
        }else{
            GameAudio.saw.resume();
            GameAudio.magont.resume();
            GameAudio.visa.resume();
        }

        if (fadingUp){
            return;
        }

        sawTime += delta;
        visaTime += delta;
        magontTime += delta;


        float sawCos = -0.1f + Math.abs(MathUtils.cos(sawTime/3));
        //GameAudio.saw.setPitch(sawId,sawCos);
        GameAudio.saw.setVolume(sawId,sawCos);

        float magontSin = 0.5f + Math.abs(MathUtils.cos(magontTime/7));
        //GameAudio.saw.setPitch(sawId,magontSin);
        GameAudio.magont.setVolume(magontId,magontSin);

        float visaCos = -0.25f + Math.min(Math.abs(MathUtils.sin(visaTime/7)),0.45f);
        GameAudio.visa.setVolume(visaId,visaCos);


        //System.out.println(sawCos + "  "+magontSin + "   "+ visaCos);
    }

    private void renderUI() {
        uiSpriteBatch.begin();
        ui.render(uiSpriteBatch);




        uiSpriteBatch.end();
    }

    private void renderLevel() {
        spriteBatch.begin();



        level.render(spriteBatch);
        Hero hero = Level.hero;
        if (inputController.isPaused()){
            if (!hero.hasDied()) {
                overlay.setColor(0f, 0, 0, 0.3f);
                overlay.setAlpha(0.7f);
            }

        }else {
            overlay.setColor(0.5f,0,0,0.3f);


            if (hero.getHealth() >= 5) {
                overlay.setAlpha(0f);
            }

            if (hero.getHealth() == 4) {
                overlay.setAlpha(0.2f);
            }

            if (hero.getHealth() == 3) {
                overlay.setAlpha(0.4f);
            }

            if (hero.getHealth() == 2) {
                overlay.setAlpha(0.6f);
            }

            if (hero.getHealth() == 1) {
                overlay.setAlpha(0.8f);
            }

            if (hero.getHealth() == 0) {
                overlay.setAlpha(1.0f);
            }
        }


        overlay.draw(spriteBatch);


        if (!hero.hasDied() && inputController.isPaused()){
            FontDrawer.drawTextAt(spriteBatch,"P A U S E D", 70, 200, 1.0f, false);
            FontDrawer.drawTextAt(spriteBatch,"CLICK TO START!", 95, 160, 0.5f, true);
        }

        if (hero.hasDied()){
            FontDrawer.drawTextAt(spriteBatch,"GAME OVER!", 80, 200, 1.0f, false);
            FontDrawer.drawTextAt(spriteBatch,"PRESS T TO TRY AGAIN!", 76, 160, 0.5f, true);

            if (Gdx.input.isKeyJustPressed(Input.Keys.T)){
                LD37.showIntro();
            }
        }


        spriteBatch.end();
    }

    @Override
    public void resize(int width, int height) {
        windowHeight = height;
        windowWidth = width;
        orthographicCamera.position.set(320/2,240/2,0);
        orthographicCamera.update();

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        GameAudio.saw.stop();
        GameAudio.visa.stop();
        GameAudio.magont.stop();
        spriteBatch.dispose();
        uiSpriteBatch.dispose();

    }
}
