package st.rhapsody.ld37.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld37.entity.Hero;
import st.rhapsody.ld37.level.Level;

/**
 * Created by nicklas on 2016-12-10.
 */
public class UI {

    private final Sprite heartSprite;
    private Level level;

    public UI(Level level) {

        this.level = level;
        heartSprite = new Sprite(new Texture(Gdx.files.internal("heart.png")));
    }

    public void tick(float delta){

    }

    public void render(SpriteBatch spriteBatch){
        renderHearts(spriteBatch);
        renderScore(spriteBatch);

    }

    private void renderScore(SpriteBatch spriteBatch) {
        Hero hero = level.hero;

        String score = String.valueOf(hero.getScore());
        int length = score.length();
        int xOffset = length*16;

        FontDrawer.drawTextAt(spriteBatch,String.valueOf(score),320-xOffset,16, 1f, false);

    }

    private void renderHearts(SpriteBatch spriteBatch) {
        Hero hero = level.hero;

        int heroHealth = hero.getHealth();

        int x = -16;
        for (int i = 0; i < heroHealth; i++) {
            x += 16;
            heartSprite.setPosition(x, 0);
            heartSprite.draw(spriteBatch);
        }
    }
}
